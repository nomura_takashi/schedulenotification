package frame;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

public class TodaysScheduleList extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTable table;
	private SystemTray tray;
	private TrayIcon icon;

	/**
	 * Launch the application.
	 * @param 今日のスケジュールが保持されているリスト
	 * @param タスクトレイに設定されてるSystemTray
	 * @param タスクトレイに設定されているアイコン
	 */
	public void Frame(List<String> todayScheduleList,SystemTray tray,TrayIcon icon){

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TodaysScheduleList frame = new TodaysScheduleList(todayScheduleList,tray,icon);
					frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @param 今日のスケジュールが保持されているリスト
	 * @param タスクトレイに設定されてるSystemTray
	 * @param タスクトレイに設定されているアイコン
	 */
	public TodaysScheduleList(List<String> todayScheduleList, SystemTray tray, TrayIcon icon) {
		//トレイの設定
		this.tray = tray;
		this.icon = icon;

		// jframeモジュールの設定
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 335, 444);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton scheduleAdd = new JButton("予定を追加");
		scheduleAdd.setFont(new Font("メイリオ", Font.PLAIN, 18));
		scheduleAdd.setBounds(72, 357, 179, 21);
		scheduleAdd.addActionListener(this);
		contentPane.add(scheduleAdd);

		String[] strList = todayScheduleList.toArray(new String[0]);
		JList<Object> list = new JList<Object>(strList);
		list.setFont(new Font("メイリオ", Font.PLAIN, 18));
		list.setSelectedIndex(1);
		list.setBounds(12, 10, 295, 385);
		contentPane.add(list);
		table = new JTable();
		table.setBounds(554, 328, -291, -195);
		contentPane.add(table);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ScheduleAdd setting = new ScheduleAdd(tray,icon);
		this.setVisible(false);
		setting.addSchedule(tray,icon);
	}
}
