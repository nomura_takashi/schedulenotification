package frame;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import main.Schedule;

public class ScheduleAdd extends JFrame  implements ActionListener, WindowListener {

	private JPanel contentPane;
	private JTextField timesTextField;
	private JTextField dateTextField;
	private JTextArea contentTextField;
	private JButton commitButton;
	ScheduleAdd frame ;
	private JComboBox<Object> beforeCombo;
	private JLabel lblNewLabel;
	private SystemTray tray;
	private TrayIcon icon;

	/**
	 * Launch the application.
	 * @param タスクトレイに設定されてるSystemTray
	 * @param タスクトレイに設定されているアイコン
	 */
	public void addSchedule(SystemTray tray,TrayIcon icon) {


		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					frame = new ScheduleAdd(tray,icon);
					frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void windowClosed(WindowEvent e) {
	}


	/**
	 * Create the frame.
	 * @param タスクトレイに設定されてるSystemTray
	 * @param タスクトレイに設定されているアイコン
	 */
	public ScheduleAdd(SystemTray tray,TrayIcon icon) {
		//トレイの設定
		this.tray = tray;
		this.icon = icon;
		setTitle("予定の追加");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 559, 450);
		addWindowListener(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel times = new JLabel("時間:");
		times.setFont(new Font("メイリオ", Font.PLAIN, 18));
		times.setBounds(33, 95, 50, 37);
		contentPane.add(times);

		timesTextField = new JTextField();
		timesTextField.setBounds(100, 91, 218, 43);
		contentPane.add(timesTextField);
		timesTextField.setColumns(10);

		JLabel timeDate = new JLabel("日付:");
		timeDate.setFont(new Font("メイリオ", Font.PLAIN, 18));
		timeDate.setBounds(33, 20, 72, 37);
		contentPane.add(timeDate);

		dateTextField = new JTextField();
		dateTextField.setBounds(100, 16, 218, 43);
		contentPane.add(dateTextField);
		dateTextField.setColumns(10);

		JLabel contents = new JLabel("内容:");
		contents.setFont(new Font("メイリオ", Font.PLAIN, 18));
		contents.setBounds(33, 181, 56, 31);
		contentPane.add(contents);

		contentTextField = new JTextArea(3, 20);
		contentTextField.setLineWrap(true);
		contentTextField.setBounds(95, 163, 223, 216);

		contentPane.add(contentTextField);
		contentTextField.setColumns(10);

		commitButton = new JButton("登録");
		commitButton.setBackground(Color.WHITE);
		commitButton.setForeground(Color.BLACK);
		commitButton.setFont(new Font("メイリオ", Font.PLAIN, 18));
		commitButton.setBounds(403, 194, 90, 37);
		commitButton.addActionListener(this);

		contentPane.add(commitButton);

		String[] combodata = {"00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50"};
		JComboBox<Object> jComboBox = new JComboBox<Object>(combodata);
		beforeCombo = jComboBox;
		beforeCombo.setBounds(347, 101, 50, 23);
		contentPane.add(beforeCombo);

		lblNewLabel = new JLabel("分前に通知");
		lblNewLabel.setFont(new Font("メイリオ", Font.PLAIN, 18));
		lblNewLabel.setBounds(403, 98, 117, 31);
		contentPane.add(lblNewLabel);


	}




	@Override
	public void windowOpened(WindowEvent e) {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO 自動生成されたメソッド・スタブ

	}


	//登録ボタンを押した時の処理

	@Override
	public void actionPerformed(ActionEvent e) {
		if(register()) {
		this.dispose();
		tray.remove(icon);
		try {
			//再読み込み
			Schedule.run();
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		}
	}



	//値取得

	public boolean register() {
		String date;
		String time;
		String timesAfter;
		String content;
		int minusData;
		String postscriptStr = "";
		//日付取得
		date = dateTextField.getText();
		JFrame frame = new JFrame();
		if(!date.matches("^([0-9][0-9][0-9][0-9])-[0-1][0-9]-[0-3][0-9]$")) {

			JOptionPane.showMessageDialog(frame, "日付の入力が不正です"+System.lineSeparator()
			+ "フォーマット例:2021-08-30");
			return false;
		}

		//予定時間取得
		time= timesTextField.getText();
		if(!time.matches("^[0-2][0-9]:[0-5][0-9]$")) {
			JOptionPane.showMessageDialog(frame, "時間の入力が不正です"+System.lineSeparator()
			+ "フォーマット例:10:20");
			return false;
		}

		//内容取得
		if((content = contentTextField.getText()).equals("")) {
			JOptionPane.showMessageDialog(frame, "内容を入力してください");
			return false;
		}
		//指定時間取得
		String str = (String) beforeCombo.getSelectedItem();
		minusData = Integer.parseInt(str)*-1;
		timesAfter =calculationDate(date, time,minusData);

		postscriptStr = date + "," + timesAfter + "," + time +"から"+ content;



		try {
			FileWriter file = new FileWriter("schedule.sct", true);
			try (PrintWriter pw = new PrintWriter(file)) {
				pw.println(postscriptStr);
				pw.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		JOptionPane.showMessageDialog(frame, "追加いたしました"+System.lineSeparator() + "内容は" + postscriptStr);
		return true;
	}


	/*
	 * 日付の計算
	 * @parm 日付
	 * @parm 時間
	 * @parm 計算用データ
	 */
	public String calculationDate(String date,String times, int minusData) {
		// 加算されるDate型の日時の作成
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd/HH:mm:ss");
		Date dateCal = new Date();
		try {
			dateCal = sdf.parse(date + "/" + times + ":00");
		} catch(ParseException e) {
			e.printStackTrace();
		}

		// Date型の日時をCalendar型に変換
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateCal);

		// 日時を計算する
		calendar.add(Calendar.MINUTE, minusData);

		// Calendar型の日時をDate型に戻す
		Date d1 = calendar.getTime();
		String str = sdf.format(d1);
		//分割
		String[] items = str.split("/");
		System.out.println(items[1] + ":00");
		return items[1];
	}

}
