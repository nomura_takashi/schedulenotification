package main;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import frame.TodaysScheduleList;

public class Schedule {

	public static void main(String[] args)throws ParseException {
		run();
	}

	//リストの定義や予定ファイルの読み込みなど
	public static void run() throws ParseException {
		//本日のスケジュールをセットするリスト
		List<String> todayScheduleList = new ArrayList<String>();

		//今日の日付を取得
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		Date dateObj = calendar.getTime();
		String todayDate = dtf.format(dateObj).substring(0,10);
		if(!readFile(todayScheduleList,dtf,dateObj)) {
			return;
		}

		// ポップアップメニューを生成
		PopupMenu popup = new PopupMenu();
		Image image = Toolkit.getDefaultToolkit().createImage( ClassLoader.getSystemResource("icon.png") ); // アイコン画像を準備
		TrayIcon icon = new TrayIcon(image, "スケジュール通知", popup);

		popupRun(popup, icon, todayScheduleList);

		for(int i = 0; i < todayScheduleList.size(); i++) {
			System.out.println(todayScheduleList.get(i));
			scheduleMesseage(icon, todayScheduleList.get(i),todayDate);
		}
	}

	/**
	 * スケジュールを読み込む
	 */
	private static boolean readFile(List<String> todayScheduleList,SimpleDateFormat dtf, Date todayDate) {
		BufferedReader br = null;
		try {
			File file = new File("schedule.sct");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				//分割
				String[] items = line.split(",");
				try {
					Date scheduleDate =dtf.parse(items[0] + " "  + items[1]);
					boolean d1 = scheduleDate.after(todayDate);
					if(d1) {
						todayScheduleList.add(items[1]+ "," +items[2]);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}catch(IOException e) {
			return false;
		}finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * システムトレイにアイコンを出すメソッド
	 * @param ポップアップメニュー
	 * @param iconを設定したTrayIcon
	 * @param 今日のスケジュールを登録したリスト
	 */
	private static void popupRun(PopupMenu popup,TrayIcon icon,List<String> todayScheduleList) {
		SystemTray tray = SystemTray.getSystemTray(); // システムトレイを取得
		icon.setImageAutoSize(true); // リサイズ

		//ポップアップメニューの中身を作成
		MenuItem item1 = new MenuItem("設定");
		item1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				TodaysScheduleList schedule = new TodaysScheduleList(todayScheduleList,tray, icon);
				schedule.Frame(todayScheduleList,tray, icon);
			}
		});

		MenuItem item2 = new MenuItem("Exit");
		item2.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				tray.remove(icon);
			}
		});
		popup.add(item1);
		popup.add(item2);
		try {
			tray.add(icon); // システムトレイに追加
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}


	/**
	 * システム通知を設定するメソッド
	 * @param iconを設定したTrayIcon
	 * @param 通知内容が格納されている変数
	 * @param スケジュールの時間が格納されている変数
	 */
	private static void scheduleMesseage(TrayIcon icon,String scheduleDate, String todayDate) throws ParseException{
		//時間設定
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
		String items[] = scheduleDate.split(",");
		final Timer timer = new Timer(false);
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				icon.displayMessage("スケジュール設定", items[1], MessageType.INFO);
				timer.cancel();
			}
		};
		timer.schedule(task, sdf.parse(todayDate+","+items[0]));
	}
}